import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { TaskServiceService } from '../services/task-service.service';
import { ITask } from '../types/Task';

@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styleUrls: ['./todo-add.component.css']
})
export class TodoAddComponent implements OnInit {
  taskForm: FormGroup = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
    description: new FormControl(''),
    date: new FormControl(new Date()),
  });

  constructor(
    private taskService: TaskServiceService
  ) { }

  ngOnInit(): void {}

  addTask(){
    if(this.taskForm.valid){

      const newTask:ITask = {
        id: 0,
        name: this.taskForm.get('name')?.value,
        description: this.taskForm.get('description')?.value,
        date: this.taskForm.get('date')?.value,
        isDone: false
      }
      this.taskService.addTask(newTask)
      this.clearForm()
    }
  }

  editTask(task:ITask){
    this.taskService.editTask(task)
  }

  removeTask(task:ITask){
    this.taskService.removeTask(task)
  }

  clearForm(){
    this.taskForm = new FormGroup({
      id: new FormControl(''),
      name: new FormControl(''),
      description: new FormControl(''),
      date: new FormControl(new Date()),
    });
  }

}

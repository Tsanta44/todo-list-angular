import { Component, Input, OnInit } from '@angular/core';
import { ITask } from '../types/Task';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {
  @Input() task:ITask = {
    id: 0,
    name: '',
    description: '',
    date: new Date()
  }

  constructor() { }

  ngOnInit(): void {}

}

import { Injectable } from '@angular/core';
import { ITask } from '../types/Task';
import * as _ from 'lodash'
import { BehaviorSubject } from 'rxjs';
import * as moment from 'moment'

@Injectable({
  providedIn: 'root'
})
export class TaskServiceService {
  tasks = new BehaviorSubject<ITask[]>([])
  all:ITask[] = []
  filterTasks:ITask[] = []
  taskIncrement = 0

  constructor() { }

  addTask(task:ITask){
    task.id = ++this.taskIncrement
    this.all = [...this.all, task]
    this.tasks.next(this.all)
  }

  editTask(task:ITask){
    const tasks = this.tasks;
    const index = task.id - 1;
    const updatedTasks = this.all.splice(index, 1, task);
    this.tasks.next(updatedTasks)
  }

  removeTask(task:ITask){
    const afterRemoveTask = this.tasks.getValue().filter(item => item.id !== task.id)
    this.tasks.next(afterRemoveTask)
  }

  search(text:string){
    if(_.isEmpty(text)){
      this.tasks.next(this.all)
    }
    const searchTasks = _.filter(this.all, (task: ITask) => _.includes(task.description.toLowerCase(), text.toLowerCase()) || _.includes(task.name.toLowerCase(), text.toLowerCase()))
    this.tasks.next(searchTasks)
  }

  searchDateRange(before:string, after:string){
    const searchTasks =  _.filter(this.all, (task: ITask) => moment(task.date).isBetween(moment(before, 'YYYY-MM-DD').toDate(), moment(after, 'YYYY-MM-DD').toDate()))
    this.tasks.next(searchTasks)
  }
}

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TaskServiceService } from '../services/task-service.service';
import { ITask } from '../types/Task';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  todos = {} as ITask[] 
  displayedColumns: string[] = ['status', 'id', 'description', 'name', 'date', 'action'];
  search = ''
  before = ''
  after = ''

  constructor(private taskService: TaskServiceService) {
    this.taskService.tasks.subscribe(todos => this.todos = todos)
   }

  ngOnInit(): void {
  }

  handleSearchChange(){
    this.taskService.search(this.search)
  }

  handleDateRange(){
    this.taskService.searchDateRange(this.before, this.after)
  }

  delete(todo:ITask){
    this.taskService.removeTask(todo)
  }

}
